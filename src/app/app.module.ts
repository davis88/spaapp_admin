import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {HttpClientModule, HttpClient} from '@angular/common/http';



import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { RecipesComponent } from './recipes/recipes.component';
import { RecipeListComponent } from './recipes/recipe-list/recipe-list.component';
import { RecipeDetailComponent } from './recipes/recipe-detail/recipe-detail.component';
import { RecipeItemComponent } from './recipes/recipe-list/recipe-item/recipe-item.component';
import { ShoppingListComponent } from './shopping-list/shopping-list.component';
import { ShoppingEditComponent } from './shopping-list/shopping-edit/shopping-edit.component';
import { DropdownDirective } from './shared/dropdown.directive';
import { ShoppingListService } from './shopping-list/shopping-list.service';
import { AppRoutingModule } from './app-routing.module';
import { RecipeStartComponent } from './recipes/recipe-start/recipe-start.component';
import { RecipeEditComponent } from './recipes/recipe-edit/recipe-edit.component';
import { RecipeService } from './recipes/recipe.service';
import { DataStorageService } from './shared/data-storage.service';
import { SignupComponent } from './auth/signup/signup.component';
import { SigninComponent } from './auth/signin/signin.component';
import { AuthService } from './auth/auth.service';
import { AuthGuard } from './auth/auth-guard.service';
import { OutletComponent } from './outlet/outlet.component';
import { OutletStartComponent } from './outlet/outlet-start/outlet-start.component';
import { OutletEditComponent } from './outlet/outlet-edit/outlet-edit.component';
import { OutletDetailComponent } from './outlet/outlet-detail/outlet-detail.component';
import { OutletListComponent } from './outlet/outlet-list/outlet-list.component';
import { StaffComponent } from './staff/staff.component';
import { StaffListComponent } from './staff/staff-list/staff-list.component';
import { StaffEditComponent } from './staff/staff-edit/staff-edit.component';
import { StaffDetailComponent } from './staff/staff-detail/staff-detail.component';
import { StaffStartComponent } from './staff/staff-start/staff-start.component';
import { StaffService } from './staff/staff.service';
import { StaffItemComponent } from './staff/staff-list/staff-item/staff-item.component';

import { CalendarModule } from 'angular-calendar';
import { StaffCalanderComponent } from './staff/staff-calander/staff-calander.component';

import { DemoUtilsModule } from './staff/demo-utils/module'
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; 

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    RecipesComponent,
    RecipeListComponent,
    RecipeDetailComponent,
    RecipeItemComponent,
    ShoppingListComponent,
    ShoppingEditComponent,
    DropdownDirective,
    RecipeStartComponent,
    RecipeEditComponent,
    SignupComponent,
    SigninComponent,
    OutletComponent,
    OutletStartComponent,
    OutletEditComponent,
    OutletDetailComponent,
    OutletListComponent,
    StaffComponent,
    StaffListComponent,
    StaffEditComponent,
    StaffDetailComponent,
    StaffStartComponent,
    StaffItemComponent,
    StaffCalanderComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    // HttpClient,
    AppRoutingModule,
    NgbModalModule.forRoot(),    
    CalendarModule.forRoot(),
    DemoUtilsModule,
    HttpClientModule
    //forRoot()
  ],
  providers: [ShoppingListService, RecipeService, DataStorageService, AuthService, AuthGuard, StaffService],
  bootstrap: [AppComponent]
})
export class AppModule { }
