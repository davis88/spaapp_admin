import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RecipesComponent } from './recipes/recipes.component';
import { ShoppingListComponent } from './shopping-list/shopping-list.component';
import { RecipeStartComponent } from './recipes/recipe-start/recipe-start.component';
import { RecipeDetailComponent } from './recipes/recipe-detail/recipe-detail.component';
import { RecipeEditComponent } from './recipes/recipe-edit/recipe-edit.component';
import { SignupComponent } from './auth/signup/signup.component';
import { SigninComponent } from './auth/signin/signin.component';
import { AuthGuard } from './auth/auth-guard.service';
import { OutletComponent } from './outlet/outlet.component';
import { OutletStartComponent } from './outlet/outlet-start/outlet-start.component';
import { OutletEditComponent } from './outlet/outlet-edit/outlet-edit.component';
import { OutletDetailComponent } from './outlet/outlet-detail/outlet-detail.component';
import { StaffComponent } from './staff/staff.component';
import { StaffStartComponent } from './staff/staff-start/staff-start.component';
import { StaffEditComponent } from './staff/staff-edit/staff-edit.component';
import { StaffDetailComponent } from './staff/staff-detail/staff-detail.component';
import { StaffCalanderComponent } from './staff/staff-calander/staff-calander.component';

const appRoutes: Routes = [
  { path: '', redirectTo: '/staff', pathMatch: 'full' },
  { path: 'recipes', component: RecipesComponent, children: [
    { path: '', component: RecipeStartComponent },
    { path: 'new', component: RecipeEditComponent, canActivate: [AuthGuard] },
    { path: ':id', component: RecipeDetailComponent },
    { path: ':id/edit', component: RecipeEditComponent, canActivate: [AuthGuard] },
  ] },
  { path: 'staff', component: StaffComponent, children: [
    { path: '', component: StaffStartComponent, canActivate: [AuthGuard] },
    { path: 'new', component: StaffEditComponent, canActivate: [AuthGuard] },
    { path: ':id', component: StaffDetailComponent, canActivate: [AuthGuard] },
    { path: ':id/edit', component: StaffEditComponent, canActivate: [AuthGuard] },
    { path: ':id/calander', component: StaffCalanderComponent, canActivate: [AuthGuard] },
  ] },
  { path: 'outlet', component: OutletComponent, children: [
    { path: '', component: OutletStartComponent, },
    { path: 'new', component: OutletEditComponent, canActivate: [AuthGuard] },
    { path: ':id', component: OutletDetailComponent, canActivate: [AuthGuard] },
    { path: ':id/edit', component: OutletEditComponent, canActivate: [AuthGuard] },
  ] },
  { path: 'shopping-list', component: ShoppingListComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'signin', component: SigninComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
