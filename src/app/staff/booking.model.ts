import { Service } from "./service.model";


export class Booking {
    
  public date: Date
  public service_id: string;
  public id: string;
  public time_slot: string;
  public staff_id: string;
  public start_time: string;
  public user_id: string;
  public service:Service

  

  constructor(date: Date, 
    service_id: string,
    email: string, 
    id: string, 
    time_slot: string, 
    staff_id: string,
    start_time: string,
    user_id: string,
    service: Service
  ) {

    this.date = date;
    this.service_id = service_id
    this.time_slot = time_slot
    this.id = id
    this.staff_id = staff_id
    this.start_time = start_time
    this.user_id = user_id
    this.service = service

  }
}
