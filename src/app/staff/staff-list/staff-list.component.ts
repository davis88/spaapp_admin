import { Component, OnInit } from '@angular/core';
import { StaffService } from '../staff.service';
import { Staff } from '../staff.model';

@Component({
  selector: 'app-staff-list',
  templateUrl: './staff-list.component.html',
  styleUrls: ['./staff-list.component.css']
})
export class StaffListComponent implements OnInit {

  constructor(
    private staffService : StaffService
  ) { }

  staffs: Staff[]

  ngOnInit() {

    this.staffService.getStaffList().subscribe(
      (res) => {
        this.staffs = res
      },
      (err) => { }
    );

  }

  

}
