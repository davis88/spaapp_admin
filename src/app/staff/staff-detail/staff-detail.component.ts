import { Component, OnInit } from '@angular/core';
import { StaffService } from '../staff.service';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { Staff } from '../staff.model';

@Component({
  selector: 'app-staff-detail',
  templateUrl: './staff-detail.component.html',
  styleUrls: ['./staff-detail.component.css']
})

export class StaffDetailComponent implements OnInit {
  staff: Staff;  
  id: number;
  
  constructor(
    private staffService: StaffService,
    private route: ActivatedRoute,
    private router:Router
  ) { }

  ngOnInit() {
    this.route.params
    .subscribe(
      (params: Params) => {
        this.id = params['id'];
        
        console.log(this.id)

        this.staffService.getStaff(this.id).subscribe(
          (res) => {
            this.staff = res
            console.log("res")
          },
          (err) => { }
        );

        
      }
    );
    
  }


  onEditStaff() {
    this.router.navigate(['edit'], {relativeTo: this.route});
    // this.router.navigate(['../', this.id, 'edit'], {relativeTo: this.route});
  }

  onDeleteStaff() {
    // this.recipeService.deleteRecipe(this.id);
    this.router.navigate(['/staff']);
  }

  onViewStaffCalander(){
    this.router.navigate(['calander'], {relativeTo: this.route});  }

}
