import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { AuthService } from '../auth/auth.service';
import { Staff } from './staff.model';
import { Observable } from 'rxjs/Observable';
import { Booking } from './booking.model';

@Injectable()
export class StaffService {

  staffs :Staff[] = []

  constructor(
      private http: Http,
      private authService: AuthService) { }

  getStaff(id): Observable<Staff> {
    return this.http.get(this.authService.getBaseUrl() + "/api/staffs/"+id)
      .map(
        res => {
          return res.json();
        });
    }

  getStaffBookings(id): Observable<Booking[]> {
      return this.http.get(this.authService.getBaseUrl() + "/api/staffs/bookings/"+id)
        .map(
          res => {
            return res.json();
          });
      }
 
  getStaffList(): Observable<Staff[]> {
    
    return this.http.get(this.authService.getBaseUrl() + "/api/staffs")
      .map(
        res => {
          return res.json();
        });
  }

}
