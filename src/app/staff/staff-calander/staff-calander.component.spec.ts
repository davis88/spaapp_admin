import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaffCalanderComponent } from './staff-calander.component';

describe('StaffCalanderComponent', () => {
  let component: StaffCalanderComponent;
  let fixture: ComponentFixture<StaffCalanderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaffCalanderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaffCalanderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
