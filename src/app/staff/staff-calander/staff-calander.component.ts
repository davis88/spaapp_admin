// // import { Component, OnInit } from '@angular/core';
// // import { StaffService } from '../staff.service';
// // import { ActivatedRoute, Params, Router } from '@angular/router';
// // import { Booking } from '../booking.model';

// // @Component({
// //   selector: 'app-staff-calander',
// //   templateUrl: './staff-calander.component.html',
// //   styleUrls: ['./staff-calander.component.css']
// // })
// // export class StaffCalanderComponent implements OnInit {
// //   id: number;
// //   bookings: Booking[];

// //   constructor(
// //     private staffService: StaffService,
// //     private route: ActivatedRoute,
// //     private router:Router
// //   ) { }

// //   ngOnInit() {
// //     this.route.params
// //     .subscribe(
// //       (params: Params) => {
// //         this.id = params['id'];
        
// //         console.log(this.id)

// //         this.staffService.getStaffBookings(this.id).subscribe(
// //           (res) => {
// //             console.log("res")
// //             this.bookings = res
// //             console.log(this.bookings)
// //           },
// //           (err) => { }
// //         );

        
// //       }
// //     );
// //   }

// // }


// // import { Component, OnInit } from '@angular/core';
// import { StaffService } from '../staff.service';
// import { ActivatedRoute, Params, Router } from '@angular/router';
// import { Booking } from '../booking.model';
// import { Component, ChangeDetectionStrategy, OnInit } from '@angular/core';
// import { HttpClient, HttpParams } from '@angular/common/http';
// import { map } from 'rxjs/operators/map';
// import { CalendarEvent } from 'angular-calendar';
// import {
//   isSameMonth,
//   isSameDay,
//   startOfMonth,
//   endOfMonth,
//   startOfWeek,
//   endOfWeek,
//   startOfDay,
//   endOfDay,
//   format
// } from 'date-fns';
// import { Observable } from 'rxjs/Observable';
// import { colors } from '../demo-utils/colors';

// import {
//   CalendarEventAction,
//   CalendarEventTimesChangedEvent
// } from 'angular-calendar';

// import {
//   subDays,
//   addDays,
//   addHours
// } from 'date-fns';
// import { AuthService } from '../../auth/auth.service';

// interface Film {
//   id: number;
//   title: string;
//   release_date: string;
// }

// interface _Booking {
//   id: number;
//   title: string;
//   time_slot: string;
//   date: string;
// }

// @Component({
//   selector: 'app-staff-calander',
//   templateUrl: './staff-calander.component.html',
//   changeDetection: ChangeDetectionStrategy.OnPush,
//   styleUrls: ['./staff-calander.component.css']
// })
// export class StaffCalanderComponent implements OnInit {
//   view: string = 'month';

//   id: number;
//   bookings: Booking[];


//   actions: CalendarEventAction[] = [
//     {
//       label: '<i class="fa fa-fw fa-pencil"></i>',
//       onClick: ({ event }: { event: CalendarEvent }): void => {
//         // this.handleEvent('Edited', event);
//       }
//     },
//     {
//       label: '<i class="fa fa-fw fa-times"></i>',
//       onClick: ({ event }: { event: CalendarEvent }): void => {
//         this.events = this.events.filter(iEvent => iEvent !== event);
//         // this.handleEvent('Deleted', event);
//       }
//     }
//   ];

//   events: CalendarEvent[] = [
//     {
//       start: subDays(startOfDay(new Date()), 1),
//       end: addDays(new Date(), 1),
//       title: 'Bala event',
//       color: colors.green,
//       actions: this.actions
//     },
//     {
//       start: startOfDay(new Date()),
//       title: 'Bala event',
//       color: colors.yellow,
//       actions: this.actions
//     },
//     {
//       start: subDays(endOfMonth(new Date()), 3),
//       end: addDays(endOfMonth(new Date()), 3),
//       title: 'A long event that spans 2 months',
//       color: colors.blue
//     },
//     {
//       start: addHours(startOfDay(new Date()), 2),
//       end: new Date(),
//       title: 'A draggable and resizable event',
//       color: colors.yellow,
//       actions: this.actions,
//       resizable: {
//         beforeStart: true,
//         afterEnd: true
//       },
//       draggable: true
//     }
//   ];



//   viewDate: Date = new Date();

//   events$: Observable<Array<CalendarEvent<{ book: Booking }>>>;

//   activeDayIsOpen: boolean = false;

//   constructor(private http: HttpClient,
//     private authService:AuthService,
//     private staffService: StaffService,
//     private route: ActivatedRoute,
//     private router:Router) {}

//   ngOnInit(): void {
//     this.fetchEvents();

//     this.route.params
//     .subscribe(
//       (params: Params) => {
//         this.id = params['id'];
        
//         console.log(this.id)
        
//         this.events$ = this.http.get(this.authService.getBaseUrl() + "/api/staffs/bookings/"+this.id) 
//         .pipe(
//           map(({ results }: { results: Booking[] }) => {
//             return results.map((book: Booking) => {
//               return {
//                 title: book.id,
//                 start: new Date(book.date),
//                 color: colors.yellow,
//                 meta: {
//                   book
//                 }
//               };
//             });
//           })
//         );
    

//         this.staffService.getStaffBookings(this.id).subscribe(
//           (res) => {
//             console.log("res")
//             this.bookings = res
//             console.log(this.bookings)

//             var _data = this.bookings;
//             console.log(_data)
  
//             for (const index in _data) {
//               var event = _data[index]
//               var start_time = new Date(event.start_time)
              
//               var timeZoneFromDB = +0.00; //time zone value from database
//               //get the timezone offset from local time in minutes
//               var tzDifference = timeZoneFromDB * 60 + start_time.getTimezoneOffset();
//               //convert the offset to milliseconds, add to targetTime, and make a new Date
//               var offsetTime = new Date(start_time.getTime() + tzDifference * 60 * 1000);
//               start_time = offsetTime
              

//               // .push(
//               //   {
//               //     title: event.service.name + " " + event.service.type, 
//               //     start: start_time,
//               //     stick: true
//               //   }
//               // )

                

//             }

//           },
//           (err) => { }
//         );

        
//       }
//     );

//   }

//   fetchEvents(): void {
//     const getStart: any = {
//       month: startOfMonth,
//       week: startOfWeek,
//       day: startOfDay
//     }[this.view];

//     const getEnd: any = {
//       month: endOfMonth,
//       week: endOfWeek,
//       day: endOfDay
//     }[this.view];

//     const params = new HttpParams()
//       .set(
//         'primary_release_date.gte',
//         format(getStart(this.viewDate), 'YYYY-MM-DD')
//       )
//       .set(
//         'primary_release_date.lte',
//         format(getEnd(this.viewDate), 'YYYY-MM-DD')
//       )
//       .set('api_key', '0ec33936a68018857d727958dca1424f');

//     // this.events$ = this.http
//     //   .get('https://api.themoviedb.org/3/discover/movie', { params })
//     //   .pipe(
//     //     map(({ results }: { results: Film[] }) => {
//     //       return results.map((film: Film) => {
//     //         return {
//     //           title: film.title,
//     //           start: new Date(film.release_date),
//     //           color: colors.yellow,
//     //           meta: {
//     //             film
//     //           }
//     //         };
//     //       });
//     //     })
//     //   );
//   }

//   dayClicked({
//     date,
//     events
//   }: {
//     date: Date;
//     events: Array<CalendarEvent<{ book: Booking }>>;
//   }): void {
//     if (isSameMonth(date, this.viewDate)) {
//       if (
//         (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
//         events.length === 0
//       ) {
//         this.activeDayIsOpen = false;
//       } else {
//         this.activeDayIsOpen = true;
//         this.viewDate = date;
//       }
//     }
//   }

//   eventClicked(event: CalendarEvent<{ book: Booking }>): void {
//     console.log("Event clicked...")
//     // window.open(
//     //   `https://www.themoviedb.org/movie/${event.meta.film.id}`,
//     //   '_blank'
//     // );
//   }
// }





// This is trial 2

import {
  Component,
  ChangeDetectionStrategy,
  ViewChild,
  TemplateRef,
  OnInit
} from '@angular/core';
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours
} from 'date-fns';
import { Subject } from 'rxjs/Subject';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent
} from 'angular-calendar';

import { StaffService } from '../staff.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Booking } from '../booking.model';


const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  }
};

@Component({
  selector: 'app-staff-calander',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './staff-calander.component.html',
  styleUrls: ['./staff-calander.component.css']
})

export class StaffCalanderComponent implements OnInit {

  id:number 
  bookings: Booking[]

  ngOnInit(): void {
    this.route.params
    .subscribe(
      (params: Params) => {
        this.id = params['id'];
        
        console.log(this.id)

        this.staffService.getStaffBookings(this.id).subscribe(
          (res) => {
            console.log("res")
            this.bookings = res
            console.log(this.bookings)

            this.bookings.forEach(element => {
              console.log(element)  
              
              var start_time = new Date(element.start_time)
              
              var timeZoneFromDB = +0.00; //time zone value from database
              //get the timezone offset from local time in minutes
              var tzDifference = timeZoneFromDB * 60 + start_time.getTimezoneOffset();
              //convert the offset to milliseconds, add to targetTime, and make a new Date
              var offsetTime = new Date(start_time.getTime() + tzDifference * 60 * 1000);
              start_time = offsetTime

              this.events.push({
                title: "Name : " + element.service.name +
                        " Type : " +element.service.type +
                        " Duration : " +element.service.duration + " Mins"
                ,
                start: start_time,
                // end: endOfDay(new Date()),
                color: colors.red,
                draggable: true,
                resizable: {
                  beforeStart: true,
                  afterEnd: true
                }
              });
              this.refresh.next();

            });
          },
          (err) => { }
        );

        
      }
    );
  
  }

  @ViewChild('modalContent') modalContent: TemplateRef<any>;

  view: string = 'month';

  viewDate: Date = new Date();

  modalData: {
    action: string;
    event: CalendarEvent;
  };

  actions: CalendarEventAction[] = [
    {
      label: '<i class="fa fa-fw fa-pencil"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.handleEvent('Edited', event);
      }
    },
    {
      label: '<i class="fa fa-fw fa-times"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.events = this.events.filter(iEvent => iEvent !== event);
        this.handleEvent('Deleted', event);
      }
    }
  ];

  refresh: Subject<any> = new Subject();

  events: CalendarEvent[] = [
    // {
    //   start: subDays(startOfDay(new Date()), 1),
    //   end: addDays(new Date(), 1),
    //   title: 'A 3 day event',
    //   color: colors.red,
    //   actions: this.actions
    // },
    // {
    //   start: startOfDay(new Date()),
    //   title: 'An event with no end date',
    //   color: colors.yellow,
    //   actions: this.actions
    // },
    // {
    //   start: subDays(endOfMonth(new Date()), 3),
    //   end: addDays(endOfMonth(new Date()), 3),
    //   title: 'A long event that spans 2 months',
    //   color: colors.blue
    // }
    // {
    //   start: addHours(startOfDay(new Date()), 2),
    //   end: new Date(),
    //   title: 'A draggable and resizable event',
    //   color: colors.yellow,
    //   actions: this.actions,
    //   resizable: {
    //     beforeStart: true,
    //     afterEnd: true
    //   },
    //   draggable: true
    // }
  ];

  activeDayIsOpen: boolean = true;

  // constructor(private modal: NgbModal) {}
  constructor(
    private modal: NgbModal,
    private staffService: StaffService,
    private route: ActivatedRoute,
    private router:Router
  ) { }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
        this.viewDate = date;
      }
    }
  }

  eventTimesChanged({
    event,
    newStart,
    newEnd
  }: CalendarEventTimesChangedEvent): void {
    event.start = newStart;
    event.end = newEnd;
    this.handleEvent('Dropped or resized', event);
    this.refresh.next();
  }

  handleEvent(action: string, event: CalendarEvent): void {
    this.modalData = { event, action };
    this.modal.open(this.modalContent, { size: 'lg' });
  }

  addEvent(): void {
    this.events.push({
      title: 'New event',
      start: startOfDay(new Date()),
      end: endOfDay(new Date()),
      color: colors.red,
      draggable: true,
      resizable: {
        beforeStart: true,
        afterEnd: true
      }
    });
    this.refresh.next();
  }
}