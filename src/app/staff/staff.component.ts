import { Component, OnInit } from '@angular/core';
import { StaffService } from './staff.service';

@Component({
  selector: 'app-staff',
  templateUrl: './staff.component.html',
  styleUrls: ['./staff.component.css']
})
export class StaffComponent implements OnInit {

  constructor(
    private staffService: StaffService) { }

  ngOnInit() {
    console.log("COMPONENT INTIALIZED>.")
    this.staffService.getStaffList()
  }

}
