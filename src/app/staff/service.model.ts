export class Service {
    
  public duration: String
  public name: string;
  public id: string;
  public type: string;
  
  constructor(id: string, 
    duration: string,
    name: string, 
    type: string, 
  ) {

    this.duration = duration
    this.id = id
    this.type = type
    this.name = name

  }
}
