import { Outlet } from "../outlet/outlet.model";

export class Staff {
  public contact: string
  public email: string;
  public id: string;
  public name: string;
  public outlet_id: string;
  public profile_url: string;
  public outlet: Outlet;

  constructor(name: string, 
    contact: string,
    email: string, 
    id: string, 
    outlet_id: string, 
    profile_url: string,
  outlet:Outlet) {

    this.name = name;
    this.contact = contact
    this.email = email
    this.id = id
    this.outlet_id = outlet_id
    this.profile_url = profile_url
    this.outlet = outlet

  }
}
