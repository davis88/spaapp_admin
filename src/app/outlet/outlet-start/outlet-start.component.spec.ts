import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutletStartComponent } from './outlet-start.component';

describe('OutletStartComponent', () => {
  let component: OutletStartComponent;
  let fixture: ComponentFixture<OutletStartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutletStartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutletStartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
